## Installing Latest Java Development Kit

1. Check  comments and instructions at  [ Install Oracle Java (JDK) 8 on Amazon EC2 Ami](https://gist.github.com/rtfpessoa/17752cbf7156bdf32c59)
For example to install JDK 8u121-b13 run :
~~~~
wget --no-cookies --header "Cookie: gpw_e24=xxx; oraclelicense=accept-securebackup-cookie;" "http://download.oracle.com/otn-pub/java/jdk/8u101-b13/jdk-8u101-linux-x64.rpm"
~~~~
2. Set JAVA_HOME enviroment variable as follows:

~~~~
# cd /etc/profile.d
# vi jdk.sh
  export JAVA_HOME=/usr/java/default
~~~~