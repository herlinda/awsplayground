#!/bin/bash
#
#
# chkconfig: 2345 80 05

. /etc/rc.d/init.d/functions
NAME="Oracle Application Express Listener"
PIDFILE="$APEX_LISTENER_HOME/apex_listener.pid"

start() {
        echo -n "Starting $NAME: "
        if [ -f $PIDFILE ]; then
                PID=`cat $PIDFILE`
                echo APEX Listener already running: $PID
                exit 2;
        else
            su - apexuser -c "cd /home/apexuser/ORDS ; java -jar ords.war > /home/apexuser/ORDS/logs/apex.log 2>/home/apexuser/ORDS/logs/apex_error.log &"
            RETVAL=$!
            echo Started PID: $RETVAL
            echo
            echo $RETVAL >>$PIDFILE
            return $RETVAL
       fi
}

status() {
        echo -n "Status $NAME: "
        if [ -f $PIDFILE ]; then
                PID=`cat $PIDFILE`
                echo APEX Listener already running: $PID
                ps -ef | grep $PID
        else
                echo APEX Listener not running
        fi
}

stop() {
        if [ -f $PIDFILE ]; then
                PID=`cat $PIDFILE`
                echo -n "Shutting down $NAME PID:$PID"
                echo
                kill $PID
                rm -f $PIDFILE
        else
                echo APEX Listener not running
        fi
        return 0
}

log() {
        tail -f /home/apexuser/ORDS/logs/apex.log
}


case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    status)
        status
        ;;
    restart)
        stop
        start
        ;;
    log)
        log
        ;;
    info)
        info
        ;;
    *)
        echo
        echo "Usage:  {start|stop|status|restart|log|info}"
        echo
        info
        exit 1
        ;;
esac
exit $?